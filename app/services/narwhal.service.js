"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var core_2 = require('../../node_modules/angular2-cookie/core');
require('rxjs/add/operator/catch');
require('rxjs/add/operator/map');
require('rxjs/add/operator/toPromise');
var NarwhalService = (function () {
    function NarwhalService(http, cookieService) {
        this.http = http;
        this.cookieService = cookieService;
        this.cachedData = [];
        this.startedCache = [];
    }
    // General helpers....
    NarwhalService.prototype.getJSONPath = function (path, additionalHeaders, params) {
        if (additionalHeaders === void 0) { additionalHeaders = {}; }
        if (params === void 0) { params = {}; }
        return this.http.get(NarwhalService.BASE_URL + "/" + path, { search: new http_1.URLSearchParams(JSON.stringify(params)), headers: this.getHeaders(additionalHeaders) }).map(function (res) { return res.json(); });
    };
    NarwhalService.prototype.postJSONPath = function (path, additionalHeaders, params) {
        if (additionalHeaders === void 0) { additionalHeaders = {}; }
        if (params === void 0) { params = {}; }
        return this.http.post(NarwhalService.BASE_URL + "/" + path, params, { headers: this.getHeaders(additionalHeaders) }).map(function (res) { return res.json(); });
    };
    NarwhalService.prototype.getOrCacheJSONPath = function (path) {
        var self = this;
        if (this.cachedData[path]) {
            // We've already loaded the blurbs.
            return Promise.resolve(this.cachedData[path]);
        }
        else if (!this.startedCache[path]) {
            // Load the blurbs in for the first time.
            this.startedCache[path] = true;
            return this.getJSONPath(path).toPromise().then(function (result) {
                self.cachedData[path] = result;
                return self.cachedData[path];
            });
        }
        else {
            // We are in the process of loading in blurbs.
            return new Promise(function (resolve, reject) {
                setTimeout(function () {
                    resolve(self.getOrCacheJSONPath(path));
                }, 200);
            });
        }
    };
    // Login routes...
    NarwhalService.prototype.postLogin = function (email, password) {
        var self = this;
        return this.http.post(NarwhalService.SERVER + "/oauth/token", {
            client_id: NarwhalService.CLIENT_ID,
            client_secret: NarwhalService.CLIENT_SECRET,
            grant_type: "password",
            username: email,
            password: password,
        }, { headers: this.getHeaders({}) }).map(function (res) { return res.json(); }).toPromise().then(function (res) {
            // Set login cookie to be the new token we received.
            self.setLoginToken(res["access_token"]);
        });
    };
    NarwhalService.prototype.postRegistration = function (params) {
        return this.postJSONPath('/public/account/register', {}, params).toPromise();
    };
    NarwhalService.prototype.deleteLogout = function () {
        var self = this;
        return this.http.post(NarwhalService.SERVER + "/oauth/revoke", {
            client_id: NarwhalService.CLIENT_ID,
            client_secret: NarwhalService.CLIENT_SECRET,
            token: self.cookieService.get('access_token'),
        }, { headers: this.getHeaders({}) }).map(function (res) { return res.json(); }).toPromise().then(function (res) {
            // Remove our login cookie.
            self.cookieService.remove('access_token');
        });
    };
    // Public API routes..
    NarwhalService.prototype.getBlurb = function (name) {
        return this.getOrCacheJSONPath('public/info/blurbs').then(function (json) { return json['items'][0][name]; });
    };
    NarwhalService.prototype.getCombinedOptionsData = function (type) {
        return this.getOrCacheJSONPath('public/options/combined').then(function (json) { return json[type]["items"]; });
    };
    NarwhalService.prototype.getPublicDashboard = function () {
        return this.getJSONPath('public/info/dashboard').toPromise();
    };
    NarwhalService.prototype.getAbout = function () {
        return this.getJSONPath('public/info/about').toPromise();
    };
    NarwhalService.prototype.getHowTo = function () {
        return this.getJSONPath('public/info/how_to').toPromise();
    };
    // Protected API routes...
    NarwhalService.prototype.getCaptures = function () {
        return this.getJSONPath('protected/captures').toPromise();
    };
    NarwhalService.prototype.getMe = function () {
        return this.getJSONPath('protected/account/me').toPromise();
    };
    NarwhalService.prototype.getAccountDashboard = function () {
        return this.getJSONPath('protected/account/dashboard').toPromise();
    };
    // More helpers...
    NarwhalService.prototype.isLoggedIn = function () {
        var token = this.getAccessToken();
        return token != null && token != "";
    };
    NarwhalService.prototype.getAccessToken = function () {
        return this.cookieService.get('access_token');
    };
    NarwhalService.prototype.setLoginToken = function (token) {
        this.cookieService.put('access_token', token);
    };
    NarwhalService.prototype.getHeaders = function (additionalHeaders) {
        var headers = new http_1.Headers();
        headers.append('Accept', 'application/json');
        headers.append("Api-Access-Token", NarwhalService.APP_TOKEN);
        if (this.isLoggedIn()) {
            headers.append("Authorization", "Bearer " + this.getAccessToken());
        }
        for (var key in additionalHeaders) {
            headers.append(key, additionalHeaders[key]);
        }
        return headers;
    };
    // private static SERVER: string = "https://narwhal.taglouisiana.com";
    NarwhalService.SERVER = 'http://localhost:4000';
    NarwhalService.API_URL = "api/v2";
    NarwhalService.BASE_URL = NarwhalService.SERVER + '/' + NarwhalService.API_URL;
    // Secret tokens.
    NarwhalService.CLIENT_ID = "d048460bebe4f7073609ba4af0cc7ca920020d347d7f819c4eec5217358fa7e8";
    NarwhalService.CLIENT_SECRET = "adbe3b04258d9f2f5d1bc8dc66d679aab7bd2270efd3a9cc6e05b4c8412f5409";
    // The Client ID and Secret hashed together.
    NarwhalService.APP_TOKEN = "140eede5529d2cce898bb0949c835616cf935db2d07f87517492a86b18edec07";
    NarwhalService.NONEMPTY_REGEX = /^.*[^\s]+.*$/;
    NarwhalService.PRESENT_OR_NULL = /(^.*[^\s]+.*$)|(^$)/;
    NarwhalService.PHONE_REGEX = /^((\d{3})(-)?(\d{3})(-)?(\d{4}))|((1\s)(\(\d{3}\)\s)(\d{3})(-)?(\d{4}))$/;
    NarwhalService.ZIP_CODE_REGEX = /^[0-9]{5}$/;
    NarwhalService.EMAIL_REGEX = /^[^@]+@[^@\.]+(\.[^@\.]+)+$/;
    NarwhalService.PASSWORD_REGEX = /^.{8,128}$/;
    NarwhalService.STATES = [
        { name: 'Alabama', abbreviation: 'AL' },
        { name: 'Alaska', abbreviation: 'AK' },
        { name: 'Arizona', abbreviation: 'AZ' },
        { name: 'Arkansas', abbreviation: 'AR' },
        { name: 'California', abbreviation: 'CA' },
        { name: 'Colorado', abbreviation: 'CO' },
        { name: 'Connecticut', abbreviation: 'CT' },
        { name: 'Delaware', abbreviation: 'DE' },
        { name: 'District of Columbia', abbreviation: 'DC' },
        { name: 'Florida', abbreviation: 'FL' },
        { name: 'Georgia', abbreviation: 'GA' },
        { name: 'Hawaii', abbreviation: 'HI' },
        { name: 'Idaho', abbreviation: 'ID' },
        { name: 'Illinois', abbreviation: 'IL' },
        { name: 'Indiana', abbreviation: 'IN' },
        { name: 'Iowa', abbreviation: 'IA' },
        { name: 'Kansas', abbreviation: 'KS' },
        { name: 'Kentucky', abbreviation: 'KY' },
        { name: 'Louisiana', abbreviation: 'LA' },
        { name: 'Maine', abbreviation: 'ME' },
        { name: 'Maryland', abbreviation: 'MD' },
        { name: 'Massachusetts', abbreviation: 'MA' },
        { name: 'Michigan', abbreviation: 'MI' },
        { name: 'Minnesota', abbreviation: 'MN' },
        { name: 'Mississippi', abbreviation: 'MS' },
        { name: 'Missouri', abbreviation: 'MO' },
        { name: 'Montana', abbreviation: 'MT' },
        { name: 'Nebraska', abbreviation: 'NE' },
        { name: 'Nevada', abbreviation: 'NV' },
        { name: 'New Hampshire', abbreviation: 'NH' },
        { name: 'New Jersey', abbreviation: 'NJ' },
        { name: 'New Mexico', abbreviation: 'NM' },
        { name: 'New York', abbreviation: 'NY' },
        { name: 'North Carolina', abbreviation: 'NC' },
        { name: 'North Dakota', abbreviation: 'ND' },
        { name: 'Ohio', abbreviation: 'OH' },
        { name: 'Oklahoma', abbreviation: 'OK' },
        { name: 'Oregon', abbreviation: 'OR' },
        { name: 'Pennsylvania', abbreviation: 'PA' },
        { name: 'Puerto Rico', abbreviation: 'PR' },
        { name: 'Rhode Island', abbreviation: 'RI' },
        { name: 'South Carolina', abbreviation: 'SC' },
        { name: 'South Dakota', abbreviation: 'SD' },
        { name: 'Tennessee', abbreviation: 'TN' },
        { name: 'Texas', abbreviation: 'TX' },
        { name: 'Utah', abbreviation: 'UT' },
        { name: 'Vermont', abbreviation: 'VT' },
        { name: 'Virginia', abbreviation: 'VA' },
        { name: 'Washington', abbreviation: 'WA' },
        { name: 'West Virginia', abbreviation: 'WV' },
        { name: 'Wisconsin', abbreviation: 'WI' },
        { name: 'Wyoming', abbreviation: 'WY' },
    ];
    NarwhalService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, core_2.CookieService])
    ], NarwhalService);
    return NarwhalService;
}());
exports.NarwhalService = NarwhalService;
//# sourceMappingURL=narwhal.service.js.map