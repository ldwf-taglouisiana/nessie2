import { Injectable } from '@angular/core'
import {Http, Response, Headers, RequestOptions, URLSearchParams, RequestOptionsArgs} from '@angular/http';
import { Observable } from '../../node_modules/rxjs/Rx';
import { CookieService } from '../../node_modules/angular2-cookie/core';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class NarwhalService {

    // private static SERVER: string = "https://narwhal.taglouisiana.com";
    private static SERVER: string = 'http://localhost:4000';
    private static API_URL: string = "api/v2";
    private static BASE_URL = NarwhalService.SERVER + '/' + NarwhalService.API_URL;

    // Secret tokens.
    private static CLIENT_ID : string = "d048460bebe4f7073609ba4af0cc7ca920020d347d7f819c4eec5217358fa7e8";
    private static CLIENT_SECRET : string = "adbe3b04258d9f2f5d1bc8dc66d679aab7bd2270efd3a9cc6e05b4c8412f5409";

    // The Client ID and Secret hashed together.
    private static APP_TOKEN : string = "140eede5529d2cce898bb0949c835616cf935db2d07f87517492a86b18edec07";

    constructor(private http : Http, private cookieService: CookieService){ }

    // General helpers....

    private getJSONPath(path : string, additionalHeaders = {}, params = {}) : Observable<JSON> {
        return this.http.get(`${NarwhalService.BASE_URL}/${path}`, {search: new URLSearchParams(JSON.stringify(params)), headers: this.getHeaders(additionalHeaders)}).map((res) => res.json());
    }

    private postJSONPath(path : string, additionalHeaders = {}, params = {}) : Observable<JSON> {
        return this.http.post(`${NarwhalService.BASE_URL}/${path}`,params, {headers:this.getHeaders(additionalHeaders)}).map((res) => res.json());
    }

    private cachedData : any[] = [];
    private startedCache : boolean[] = [];

    private getOrCacheJSONPath(path : string) : Promise<any> {
        var self = this;
        if (this.cachedData[path]){
            // We've already loaded the blurbs.
            return Promise.resolve(this.cachedData[path]);
        }
        else if (!this.startedCache[path]){
            // Load the blurbs in for the first time.
            this.startedCache[path] = true;
            return this.getJSONPath(path).toPromise().then((result) => {
                self.cachedData[path] = result; return self.cachedData[path];
            });
        }
        else {
            // We are in the process of loading in blurbs.
            return new Promise<string>((resolve,reject) => {
                setTimeout(function(){
                    resolve(self.getOrCacheJSONPath(path));
                },200);
            });
        }
    }

    // Login routes...

    public postLogin(email : string, password : string) : Promise<any> {
       let self = this;
       return this.http.post(`${NarwhalService.SERVER}/oauth/token`,{
        client_id: NarwhalService.CLIENT_ID,
        client_secret: NarwhalService.CLIENT_SECRET,
        grant_type: "password",
        username: email,
        password: password,
       },{headers : this.getHeaders({})}).map(res => res.json()).toPromise().then((res) => {
           // Set login cookie to be the new token we received.
           self.setLoginToken(res["access_token"])
       });
    }

    public postRegistration(params) : Promise<any> {
        return this.postJSONPath('/public/account/register',{},params).toPromise();
    }

    public deleteLogout() : Promise<any> {
        let self = this;
        return this.http.post(`${NarwhalService.SERVER}/oauth/revoke`,{
            client_id: NarwhalService.CLIENT_ID,
            client_secret: NarwhalService.CLIENT_SECRET,
            token: self.cookieService.get('access_token'),
        },{headers: this.getHeaders({})}).map(res => res.json()).toPromise().then((res) => {
            // Remove our login cookie.
            self.cookieService.remove('access_token');
        });
    }

    // Public API routes..

    public getBlurb(name : string) : Promise<string> {
        return this.getOrCacheJSONPath('public/info/blurbs').then((json) => json['items'][0][name]);
    }

    public getCombinedOptionsData(type : string) : Promise<any> {
        return this.getOrCacheJSONPath('public/options/combined').then((json) => json[type]["items"]);
    }

    public getPublicDashboard(): Promise<any> {
        return this.getJSONPath('public/info/dashboard').toPromise();
    }

    public getAbout(): Promise<any>{
        return this.getJSONPath('public/info/about').toPromise();
    }

    public getHowTo(): Promise<any>{
        return this.getJSONPath('public/info/how_to').toPromise();
    }

    // Protected API routes...

    public getCaptures(): Promise<any> {
        return this.getJSONPath('protected/captures').toPromise();
    }

    public getMe(): Promise<any>{
        return this.getJSONPath('protected/account/me').toPromise();
    }

    public getAccountDashboard(): Promise<any> {
        return this.getJSONPath('protected/account/dashboard').toPromise();
    }

    // More helpers...

    public isLoggedIn(): boolean {
        let token : string = this.getAccessToken();
        return token != null && token != "";
    }

    private getAccessToken(): string {
        return this.cookieService.get('access_token');
    }

    private setLoginToken(token: string) {
        this.cookieService.put('access_token',token);
    }

    private getHeaders(additionalHeaders) : Headers {
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append("Api-Access-Token", NarwhalService.APP_TOKEN);
        if (this.isLoggedIn()){
           headers.append("Authorization","Bearer " + this.getAccessToken());
        }
        for (var key in additionalHeaders) {
            headers.append(key,additionalHeaders[key]);
        }
        return headers;
    }

    public static NONEMPTY_REGEX : RegExp = /^.*[^\s]+.*$/;
    public static PRESENT_OR_NULL : RegExp = /(^.*[^\s]+.*$)|(^$)/;
    public static PHONE_REGEX : RegExp = /^((\d{3})(-)?(\d{3})(-)?(\d{4}))|((1\s)(\(\d{3}\)\s)(\d{3})(-)?(\d{4}))$/;
	public static ZIP_CODE_REGEX : RegExp = /^[0-9]{5}$/;
	public static EMAIL_REGEX : RegExp = /^[^@]+@[^@\.]+(\.[^@\.]+)+$/;
	public static PASSWORD_REGEX : RegExp = /^.{8,128}$/;

    public static STATES : Object[] = [
        {name: 'Alabama', abbreviation: 'AL'},
        {name: 'Alaska', abbreviation: 'AK'},
        {name: 'Arizona', abbreviation: 'AZ'},
        {name: 'Arkansas', abbreviation: 'AR'},
        {name: 'California', abbreviation: 'CA'},
        {name: 'Colorado', abbreviation: 'CO'},
        {name: 'Connecticut', abbreviation: 'CT'},
        {name: 'Delaware', abbreviation: 'DE'},
        {name: 'District of Columbia', abbreviation: 'DC'},
        {name: 'Florida', abbreviation: 'FL'},
        {name: 'Georgia', abbreviation: 'GA'},
        {name: 'Hawaii', abbreviation: 'HI'},
        {name: 'Idaho', abbreviation: 'ID'},
        {name: 'Illinois', abbreviation: 'IL'},
        {name: 'Indiana', abbreviation: 'IN'},
        {name: 'Iowa', abbreviation: 'IA'},
        {name: 'Kansas', abbreviation: 'KS'},
        {name: 'Kentucky', abbreviation: 'KY'},
        {name: 'Louisiana', abbreviation: 'LA'},
        {name: 'Maine', abbreviation: 'ME'},
        {name: 'Maryland', abbreviation: 'MD'},
        {name: 'Massachusetts', abbreviation: 'MA'},
        {name: 'Michigan', abbreviation: 'MI'},
        {name: 'Minnesota', abbreviation: 'MN'},
        {name: 'Mississippi', abbreviation: 'MS'},
        {name: 'Missouri', abbreviation: 'MO'},
        {name: 'Montana', abbreviation: 'MT'},
        {name: 'Nebraska', abbreviation: 'NE'},
        {name: 'Nevada', abbreviation: 'NV'},
        {name: 'New Hampshire', abbreviation: 'NH'},
        {name: 'New Jersey', abbreviation: 'NJ'},
        {name: 'New Mexico', abbreviation: 'NM'},
        {name: 'New York', abbreviation: 'NY'},
        {name: 'North Carolina', abbreviation: 'NC'},
        {name: 'North Dakota', abbreviation: 'ND'},
        {name: 'Ohio', abbreviation: 'OH'},
        {name: 'Oklahoma', abbreviation: 'OK'},
        {name: 'Oregon', abbreviation: 'OR'},
        {name: 'Pennsylvania', abbreviation: 'PA'},
        {name: 'Puerto Rico', abbreviation: 'PR'},
        {name: 'Rhode Island', abbreviation: 'RI'},
        {name: 'South Carolina', abbreviation: 'SC'},
        {name: 'South Dakota', abbreviation: 'SD'},
        {name: 'Tennessee', abbreviation: 'TN'},
        {name: 'Texas', abbreviation: 'TX'},
        {name: 'Utah', abbreviation: 'UT'},
        {name: 'Vermont', abbreviation: 'VT'},
        {name: 'Virginia', abbreviation: 'VA'},
        {name: 'Washington', abbreviation: 'WA'},
        {name: 'West Virginia', abbreviation: 'WV'},
        {name: 'Wisconsin', abbreviation: 'WI'},
        {name: 'Wyoming', abbreviation: 'WY'},
    ];


}