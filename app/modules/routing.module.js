"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var page_not_found_component_1 = require('./../pages/page-not-found.component');
var home_page_component_1 = require('./../pages/home-page.component');
var register_page_component_1 = require('./../pages/register-page.component');
var dashboard_page_component_1 = require('./../pages/dashboard-page.component');
var info_pages_component_1 = require('./../pages/info-pages.component');
var login_page_component_1 = require('./../pages/login-page.component');
var narwhal_service_1 = require('./../services/narwhal.service');
var AuthGuard = (function () {
    function AuthGuard(router, narwhalService) {
        this.router = router;
        this.narwhalService = narwhalService;
    }
    AuthGuard.prototype.canActivate = function () {
        if (this.narwhalService.isLoggedIn()) {
            return true;
        }
        this.router.navigate(['/login']);
        return true;
    };
    AuthGuard = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [router_1.Router, narwhal_service_1.NarwhalService])
    ], AuthGuard);
    return AuthGuard;
}());
exports.AuthGuard = AuthGuard;
var RoutingModule = (function () {
    function RoutingModule() {
    }
    RoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule.forRoot([
                    // Home and special pages.
                    { path: '', component: home_page_component_1.HomePageComponent },
                    { path: 'login', component: login_page_component_1.LoginPageComponent },
                    { path: 'register', component: register_page_component_1.RegisterPageComponent },
                    // Public pages.
                    { path: 'about', component: info_pages_component_1.AboutPageComponent },
                    { path: 'how_to_guide', component: info_pages_component_1.HowToPageComponent },
                    { path: 'privacy', component: info_pages_component_1.PrivacyPageComponent },
                    // Protected pages.
                    { path: 'dashboard', component: dashboard_page_component_1.DashboardPageComponent, canActivate: [AuthGuard] },
                    // Other pages.
                    { path: '**', component: page_not_found_component_1.PageNotFoundComponent }
                ])
            ],
            exports: [
                router_1.RouterModule
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], RoutingModule);
    return RoutingModule;
}());
exports.RoutingModule = RoutingModule;
//# sourceMappingURL=routing.module.js.map