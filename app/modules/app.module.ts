import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent }   from './../components/app.component';
import { RoutingModule }   from './routing.module';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormGroup, FormControl, Validators, FormBuilder, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CookieService } from './../../node_modules/angular2-cookie/core';

import { PublicDashboardComponent, PublicDashboardSetComponent }   from './../components/public-dashboard.component';
import { AccountDashboardComponent }   from './../components/account-dashboard.component';
import { AboutComponent, HowToComponent, PrivacyComponent }   from './../components/info.component';
import { AboutPageComponent, HowToPageComponent, PrivacyPageComponent }   from './../pages/info-pages.component';
import { AccountComponent } from '../components/account.component';
import { SidePanelComponent } from './../components/side-panel.component';
import { LoginController }   from './../components/login-form.component';
import { NarwhalService }   from './../services/narwhal.service';
import { PageNotFoundComponent }   from './../pages/page-not-found.component';
import { HomePageComponent }   from './../pages/home-page.component';
import { DashboardPageComponent } from './../pages/dashboard-page.component';
import { LoginPageComponent } from './../pages/login-page.component';
import { AuthGuard } from "./routing.module";
import { RegisterPageComponent } from "../pages/register-page.component";
import { RegisterController } from "../components/register-form.component";

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    RoutingModule,
    ReactiveFormsModule
  ],
  declarations: [
    AppComponent,
    PublicDashboardComponent, AccountDashboardComponent,
    PublicDashboardSetComponent,
    DashboardPageComponent,
    AccountComponent,
    SidePanelComponent,
    LoginPageComponent,
    RegisterPageComponent,
    RegisterController,
    AboutPageComponent, AboutComponent,
    HowToPageComponent, HowToComponent,
    PrivacyPageComponent, PrivacyComponent,
    LoginController,
    HomePageComponent,
    PageNotFoundComponent
  ],
  providers: [
    NarwhalService,
    CookieService,
    AuthGuard
  ],
  bootstrap: [
    AppComponent
   ]
})
export class AppModule {

}