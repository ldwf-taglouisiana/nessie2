import { NgModule, Injectable }     from '@angular/core';
import { RouterModule, Routes, CanActivate, Router } from '@angular/router';

import { PageNotFoundComponent }   from './../pages/page-not-found.component';
import { HomePageComponent }   from './../pages/home-page.component';
import { RegisterPageComponent }   from './../pages/register-page.component';
import { DashboardPageComponent }   from './../pages/dashboard-page.component';
import { HowToPageComponent, AboutPageComponent, PrivacyPageComponent }   from './../pages/info-pages.component';
import { LoginPageComponent }   from './../pages/login-page.component';

import { NarwhalService } from './../services/narwhal.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router : Router, private narwhalService: NarwhalService) { }

    canActivate() :boolean {
        if (this.narwhalService.isLoggedIn()){
          return true;
        }
        this.router.navigate(['/login']);
        return true;
    }
}

@NgModule({
  imports: [
    RouterModule.forRoot([
      // Home and special pages.
      { path: '', component: HomePageComponent },
      { path: 'login', component: LoginPageComponent },
      { path: 'register', component: RegisterPageComponent },

      // Public pages.
      { path: 'about', component: AboutPageComponent },
      { path: 'how_to_guide', component: HowToPageComponent },
      { path: 'privacy', component: PrivacyPageComponent },

      // Protected pages.
      { path: 'dashboard', component: DashboardPageComponent, canActivate: [AuthGuard]},

      // Other pages.
      { path: '**', component: PageNotFoundComponent }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class RoutingModule {}