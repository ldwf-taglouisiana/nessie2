"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var app_component_1 = require('./../components/app.component');
var routing_module_1 = require('./routing.module');
var http_1 = require('@angular/http');
var forms_1 = require('@angular/forms');
var core_2 = require('./../../node_modules/angular2-cookie/core');
var public_dashboard_component_1 = require('./../components/public-dashboard.component');
var account_dashboard_component_1 = require('./../components/account-dashboard.component');
var info_component_1 = require('./../components/info.component');
var info_pages_component_1 = require('./../pages/info-pages.component');
var account_component_1 = require('../components/account.component');
var side_panel_component_1 = require('./../components/side-panel.component');
var login_form_component_1 = require('./../components/login-form.component');
var narwhal_service_1 = require('./../services/narwhal.service');
var page_not_found_component_1 = require('./../pages/page-not-found.component');
var home_page_component_1 = require('./../pages/home-page.component');
var dashboard_page_component_1 = require('./../pages/dashboard-page.component');
var login_page_component_1 = require('./../pages/login-page.component');
var routing_module_2 = require("./routing.module");
var register_page_component_1 = require("../pages/register-page.component");
var register_form_component_1 = require("../components/register-form.component");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                http_1.HttpModule,
                routing_module_1.RoutingModule,
                forms_1.ReactiveFormsModule
            ],
            declarations: [
                app_component_1.AppComponent,
                public_dashboard_component_1.PublicDashboardComponent, account_dashboard_component_1.AccountDashboardComponent,
                public_dashboard_component_1.PublicDashboardSetComponent,
                dashboard_page_component_1.DashboardPageComponent,
                account_component_1.AccountComponent,
                side_panel_component_1.SidePanelComponent,
                login_page_component_1.LoginPageComponent,
                register_page_component_1.RegisterPageComponent,
                register_form_component_1.RegisterController,
                info_pages_component_1.AboutPageComponent, info_component_1.AboutComponent,
                info_pages_component_1.HowToPageComponent, info_component_1.HowToComponent,
                info_pages_component_1.PrivacyPageComponent, info_component_1.PrivacyComponent,
                login_form_component_1.LoginController,
                home_page_component_1.HomePageComponent,
                page_not_found_component_1.PageNotFoundComponent
            ],
            providers: [
                narwhal_service_1.NarwhalService,
                core_2.CookieService,
                routing_module_2.AuthGuard
            ],
            bootstrap: [
                app_component_1.AppComponent
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map