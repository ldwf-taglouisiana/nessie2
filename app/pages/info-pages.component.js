"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
// How To Page
var HowToPageComponent = (function () {
    function HowToPageComponent() {
    }
    HowToPageComponent = __decorate([
        core_1.Component({
            selector: 'how-to-page',
            template: "\n    <how-to></how-to>\n  "
        }), 
        __metadata('design:paramtypes', [])
    ], HowToPageComponent);
    return HowToPageComponent;
}());
exports.HowToPageComponent = HowToPageComponent;
// About Page
var AboutPageComponent = (function () {
    function AboutPageComponent() {
    }
    AboutPageComponent = __decorate([
        core_1.Component({
            selector: '',
            template: "\n    <about></about>\n  "
        }), 
        __metadata('design:paramtypes', [])
    ], AboutPageComponent);
    return AboutPageComponent;
}());
exports.AboutPageComponent = AboutPageComponent;
// Privacy Page
var PrivacyPageComponent = (function () {
    function PrivacyPageComponent() {
    }
    PrivacyPageComponent = __decorate([
        core_1.Component({
            selector: '',
            template: "\n    <privacy></privacy>\n  "
        }), 
        __metadata('design:paramtypes', [])
    ], PrivacyPageComponent);
    return PrivacyPageComponent;
}());
exports.PrivacyPageComponent = PrivacyPageComponent;
//# sourceMappingURL=info-pages.component.js.map