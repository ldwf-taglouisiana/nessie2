import { Component } from '@angular/core';

// How To Page

@Component({
  selector: 'how-to-page',
  template:`
    <how-to></how-to>
  `
})
export class HowToPageComponent { }

// About Page

@Component({
  selector: '',
  template:`
    <about></about>
  `
})
export class AboutPageComponent { }


// Privacy Page

@Component({
  selector: '',
  template:`
    <privacy></privacy>
  `
})
export class PrivacyPageComponent { }

