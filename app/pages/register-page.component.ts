import { Component } from '@angular/core';

@Component({
  selector: 'register-page',
  template:`
    <register-form></register-form>
  `
})
export class RegisterPageComponent {

}
