import { Component } from '@angular/core';
import { NarwhalService } from './../services/narwhal.service';

@Component({
  selector: 'home-page',
  template: `
    <login-form></login-form>
  `
})
export class HomePageComponent {

  constructor (private narwhalService : NarwhalService){ }

}

