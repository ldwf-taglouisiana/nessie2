import { Component } from '@angular/core';
import { NarwhalService } from './../services/narwhal.service';

@Component({
  selector: 'dashboard-page',
  template:`
    <public-dashboard></public-dashboard>
    <account-dashboard></account-dashboard>
    <side-panel></side-panel>
    <account></account>
  `
})
export class DashboardPageComponent {

  constructor (private narwhalService : NarwhalService){ }

}
