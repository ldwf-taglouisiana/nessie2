import { Component } from '@angular/core';
import { NarwhalService } from '../services/narwhal.service';

@Component({
  selector: 'how-to-page',
  template:`
    <div [innerHTML]="narwhalService.getBlurb('login')"></div>
    <login-form></login-form>
  `
})
export class LoginPageComponent {

    constructor (private narwhalService : NarwhalService){}

}
