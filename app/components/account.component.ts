import { Component, OnInit } from '@angular/core';
import { NarwhalService } from '../services/narwhal.service';

@Component({
  selector: 'account',
  template:`
     <div [innerHTML]="me | json"></div>
  `
})
export class AccountComponent implements OnInit {

    private me = {};
    constructor(private narwhalService : NarwhalService){}

    ngOnInit() {
        this.narwhalService.getMe().then((json) => this.me = json);
    }
}
