import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core'
import { NarwhalService } from './../services/narwhal.service'

////////////////////////////////
/* Dashboard Set */
////////////////////////////////

@Component({
    selector: 'public-dashboard-set',
    template: `
        <div>{{name}} this season</div>
        <div>{{data.season?.current}}</div>
        <div *ngIf="data.species">
            <div>Program {{name}} Last 30 days</div>
            <table>
                <tr>
                    <th>Species</th>
                    <th>Count</th>
                </tr>
                <tbody>
                    <tr *ngFor="let species of data.species">
                        <td>{{species.name}}</td>
                        <td>{{species.count}}</td>
                    </tr>
                    <tr class="total">
                        <td>Total</td>
                        <td>{{currentTotal()}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    `,
})
export class PublicDashboardSetComponent {

    @Input() public name: string = '...';
    @Input() public data: any = {total: '', species: [], season: {current: '', last: ''} };

    /** Compute the total captured for all species over the last 30 days. */
    currentTotal() : number {
        var total : number = 0;
        for (var i : number = 0; i < this.data.species.length; i++){
            total += this.data.species[i].count;
        }
        return total;
    }
}

////////////////////////////////
/* Dashboard */
////////////////////////////////

@Component({
    selector: 'public-dashboard',
    template: `
        <public-dashboard-set [data]="data.captures" name="Captures"></public-dashboard-set>
        <public-dashboard-set [data]="data.recaptures" name="Recaptures"></public-dashboard-set>
    `,
})
export class PublicDashboardComponent implements OnInit {

    public data : any = {captures: {}, recaptures: {}};

    constructor (private narwhalService : NarwhalService){ }

    ngOnInit(){
        this.narwhalService.getPublicDashboard().then((json) => {
            this.data = json;
        }).catch((e) => console.log(e));
    }
}
