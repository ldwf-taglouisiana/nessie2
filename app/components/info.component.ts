import { Component, OnInit } from '@angular/core';
import { NarwhalService } from './../services/narwhal.service'

// ABOUT

@Component({
  selector: 'about',
  template: `
    <div *ngFor="let item of json.items">
        <div [innerHTML]="item.text"></div>
    </div>
  `,
})
export class AboutComponent implements OnInit {
    private json : any = {count: 0, items:[]};
    constructor(private narwhalService : NarwhalService){ }
    ngOnInit() {
        this.narwhalService.getAbout().then((json) => { this.json = json; }).catch((e) => console.log(e));
    }
}

// HOW-TO

@Component({
  selector: 'how-to',
  template: `
    <div *ngFor="let item of json.items">
        <div [innerHTML]="item.text"></div>
    </div>
  `,
})
export class HowToComponent implements OnInit {
    private json : any = {count: 0, items:[]};
    constructor(private narwhalService : NarwhalService){ }
    ngOnInit() {
        this.narwhalService.getHowTo().then((json) => { this.json = json; }).catch((e) => console.log(e));
    }
}

// PRIVACY

@Component({
  selector: 'privacy',
  template: `
    <div [innerHTML]="text"></div>
  `,
})
export class PrivacyComponent implements OnInit {
    private text : string = "";
    constructor(private narwhalService : NarwhalService){ }
    ngOnInit(){
        this.narwhalService.getBlurb("privacy_policy").then((blurb) => {this.text = blurb});
    }
}