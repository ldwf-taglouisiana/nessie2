"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var narwhal_service_1 = require('./../services/narwhal.service');
// ABOUT
var AboutComponent = (function () {
    function AboutComponent(narwhalService) {
        this.narwhalService = narwhalService;
        this.json = { count: 0, items: [] };
    }
    AboutComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.narwhalService.getAbout().then(function (json) { _this.json = json; }).catch(function (e) { return console.log(e); });
    };
    AboutComponent = __decorate([
        core_1.Component({
            selector: 'about',
            template: "\n    <div *ngFor=\"let item of json.items\">\n        <div [innerHTML]=\"item.text\"></div>\n    </div>\n  ",
        }), 
        __metadata('design:paramtypes', [narwhal_service_1.NarwhalService])
    ], AboutComponent);
    return AboutComponent;
}());
exports.AboutComponent = AboutComponent;
// HOW-TO
var HowToComponent = (function () {
    function HowToComponent(narwhalService) {
        this.narwhalService = narwhalService;
        this.json = { count: 0, items: [] };
    }
    HowToComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.narwhalService.getHowTo().then(function (json) { _this.json = json; }).catch(function (e) { return console.log(e); });
    };
    HowToComponent = __decorate([
        core_1.Component({
            selector: 'how-to',
            template: "\n    <div *ngFor=\"let item of json.items\">\n        <div [innerHTML]=\"item.text\"></div>\n    </div>\n  ",
        }), 
        __metadata('design:paramtypes', [narwhal_service_1.NarwhalService])
    ], HowToComponent);
    return HowToComponent;
}());
exports.HowToComponent = HowToComponent;
// PRIVACY
var PrivacyComponent = (function () {
    function PrivacyComponent(narwhalService) {
        this.narwhalService = narwhalService;
        this.text = "";
    }
    PrivacyComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.narwhalService.getBlurb("privacy_policy").then(function (blurb) { _this.text = blurb; });
    };
    PrivacyComponent = __decorate([
        core_1.Component({
            selector: 'privacy',
            template: "\n    <div [innerHTML]=\"text\"></div>\n  ",
        }), 
        __metadata('design:paramtypes', [narwhal_service_1.NarwhalService])
    ], PrivacyComponent);
    return PrivacyComponent;
}());
exports.PrivacyComponent = PrivacyComponent;
//# sourceMappingURL=info.component.js.map