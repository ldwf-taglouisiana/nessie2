"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var narwhal_service_1 = require('./../services/narwhal.service');
////////////////////////////////
/* Dashboard Set */
////////////////////////////////
var PublicDashboardSetComponent = (function () {
    function PublicDashboardSetComponent() {
        this.name = '...';
        this.data = { total: '', species: [], season: { current: '', last: '' } };
    }
    /** Compute the total captured for all species over the last 30 days. */
    PublicDashboardSetComponent.prototype.currentTotal = function () {
        var total = 0;
        for (var i = 0; i < this.data.species.length; i++) {
            total += this.data.species[i].count;
        }
        return total;
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], PublicDashboardSetComponent.prototype, "name", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], PublicDashboardSetComponent.prototype, "data", void 0);
    PublicDashboardSetComponent = __decorate([
        core_1.Component({
            selector: 'public-dashboard-set',
            template: "\n        <div>{{name}} this season</div>\n        <div>{{data.season?.current}}</div>\n        <div *ngIf=\"data.species\">\n            <div>Program {{name}} Last 30 days</div>\n            <table>\n                <tr>\n                    <th>Species</th>\n                    <th>Count</th>\n                </tr>\n                <tbody>\n                    <tr *ngFor=\"let species of data.species\">\n                        <td>{{species.name}}</td>\n                        <td>{{species.count}}</td>\n                    </tr>\n                    <tr class=\"total\">\n                        <td>Total</td>\n                        <td>{{currentTotal()}}</td>\n                    </tr>\n                </tbody>\n            </table>\n        </div>\n    ",
        }), 
        __metadata('design:paramtypes', [])
    ], PublicDashboardSetComponent);
    return PublicDashboardSetComponent;
}());
exports.PublicDashboardSetComponent = PublicDashboardSetComponent;
////////////////////////////////
/* Dashboard */
////////////////////////////////
var PublicDashboardComponent = (function () {
    function PublicDashboardComponent(narwhalService) {
        this.narwhalService = narwhalService;
        this.data = { captures: {}, recaptures: {} };
    }
    PublicDashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.narwhalService.getPublicDashboard().then(function (json) {
            _this.data = json;
        }).catch(function (e) { return console.log(e); });
    };
    PublicDashboardComponent = __decorate([
        core_1.Component({
            selector: 'public-dashboard',
            template: "\n        <public-dashboard-set [data]=\"data.captures\" name=\"Captures\"></public-dashboard-set>\n        <public-dashboard-set [data]=\"data.recaptures\" name=\"Recaptures\"></public-dashboard-set>\n    ",
        }), 
        __metadata('design:paramtypes', [narwhal_service_1.NarwhalService])
    ], PublicDashboardComponent);
    return PublicDashboardComponent;
}());
exports.PublicDashboardComponent = PublicDashboardComponent;
//# sourceMappingURL=public-dashboard.component.js.map