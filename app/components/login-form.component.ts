import { Component } from '@angular/core';
import { NarwhalService } from './../services/narwhal.service'
import { FormGroup, FormControl, Validators, FormBuilder, ReactiveFormsModule, FormsModule } from '@angular/forms';

@Component({
  selector: 'login-form',
  template: `
    <div *ngIf="narwhalService.isLoggedIn()">
        <form [formGroup]="logoutFormGroup" (ngSubmit)="logout()">
            <button type="submit" value="Log Out">Log Out</button>
            <div>{{error}}</div>
        </form>
    </div>
    <div *ngIf="!narwhalService.isLoggedIn()">
      <form [formGroup]="loginFormGroup" (ngSubmit)="login()">
        <input type="text" formControlName="email" >
        <input type="password" formControlName="password" >
        <button type="submit" value="Log In">Log In</button>
        <div>{{error}}</div>
      </form>
    </div>
  `,
})
export class LoginController {

    private loginFormGroup : FormGroup;
    private logoutFormGroup : FormGroup;
    private email : FormControl = new FormControl("",Validators.required);
    private password : FormControl = new FormControl("",Validators.required);
    private error: string;

    constructor(fb: FormBuilder, private narwhalService : NarwhalService){
        this.loginFormGroup = fb.group({"email": this.email,"password": this.password});
        this.logoutFormGroup = fb.group({});
        this.error = null;
    }

    login(){
       this.error = null;
       this.narwhalService.postLogin(this.email.value,this.password.value).catch((error) => {this.error = error; console.log(error)});
    }

    logout(){
        this.error = null;
        console.log(this);
        console.log(this.narwhalService);
        this.narwhalService.deleteLogout().catch((error) => this.error = error);
    }

}
