import { Component, OnInit } from '@angular/core';
import { NarwhalService } from '../services/narwhal.service';

@Component({
  selector: 'account-dashboard',
  template:`
     <div [innerHTML]="data | json"></div>
  `
})
export class AccountDashboardComponent implements OnInit {

    private data = {};
    constructor(private narwhalService : NarwhalService){}
    ngOnInit() {
        this.narwhalService.getAccountDashboard().then((json) => this.data = json);
    }
}
