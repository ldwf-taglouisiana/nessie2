"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var narwhal_service_1 = require('./../services/narwhal.service');
var forms_1 = require('@angular/forms');
var LoginController = (function () {
    function LoginController(fb, narwhalService) {
        this.narwhalService = narwhalService;
        this.email = new forms_1.FormControl("", forms_1.Validators.required);
        this.password = new forms_1.FormControl("", forms_1.Validators.required);
        this.loginFormGroup = fb.group({ "email": this.email, "password": this.password });
        this.logoutFormGroup = fb.group({});
        this.error = null;
    }
    LoginController.prototype.login = function () {
        var _this = this;
        this.error = null;
        this.narwhalService.postLogin(this.email.value, this.password.value).catch(function (error) { _this.error = error; console.log(error); });
    };
    LoginController.prototype.logout = function () {
        var _this = this;
        this.error = null;
        console.log(this);
        console.log(this.narwhalService);
        this.narwhalService.deleteLogout().catch(function (error) { return _this.error = error; });
    };
    LoginController = __decorate([
        core_1.Component({
            selector: 'login-form',
            template: "\n    <div *ngIf=\"narwhalService.isLoggedIn()\">\n        <form [formGroup]=\"logoutFormGroup\" (ngSubmit)=\"logout()\">\n            <button type=\"submit\" value=\"Log Out\">Log Out</button>\n            <div>{{error}}</div>\n        </form>\n    </div>\n    <div *ngIf=\"!narwhalService.isLoggedIn()\">\n      <form [formGroup]=\"loginFormGroup\" (ngSubmit)=\"login()\">\n        <input type=\"text\" formControlName=\"email\" >\n        <input type=\"password\" formControlName=\"password\" >\n        <button type=\"submit\" value=\"Log In\">Log In</button>\n        <div>{{error}}</div>\n      </form>\n    </div>\n  ",
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, narwhal_service_1.NarwhalService])
    ], LoginController);
    return LoginController;
}());
exports.LoginController = LoginController;
//# sourceMappingURL=login-form.component.js.map