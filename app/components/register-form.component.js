"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var narwhal_service_1 = require('./../services/narwhal.service');
var forms_1 = require('@angular/forms');
var RegisterController = (function () {
    function RegisterController(fb, narwhalService) {
        this.fb = fb;
        this.narwhalService = narwhalService;
        this.shirtSizes = [];
        this.states = narwhal_service_1.NarwhalService.STATES;
        this.submitted = false;
    }
    RegisterController.prototype.ngOnInit = function () {
        var _this = this;
        this.narwhalService.getCombinedOptionsData('shirt_sizes').then(function (json) { return _this.shirtSizes = json; });
        this.registrationFormGroup = this.fb.group({
            angler: this.fb.group({
                first_name: new forms_1.FormControl("", [forms_1.Validators.required, forms_1.Validators.pattern(narwhal_service_1.NarwhalService.NONEMPTY_REGEX.source)]),
                last_name: new forms_1.FormControl("", [forms_1.Validators.required, forms_1.Validators.pattern(narwhal_service_1.NarwhalService.NONEMPTY_REGEX.source)]),
                street: new forms_1.FormControl("", [forms_1.Validators.pattern(narwhal_service_1.NarwhalService.NONEMPTY_REGEX.source)]),
                suite: new forms_1.FormControl("", [forms_1.Validators.pattern(narwhal_service_1.NarwhalService.NONEMPTY_REGEX.source)]),
                city: new forms_1.FormControl("", [forms_1.Validators.pattern(narwhal_service_1.NarwhalService.NONEMPTY_REGEX.source)]),
                state: new forms_1.FormControl("", [forms_1.Validators.pattern(narwhal_service_1.NarwhalService.NONEMPTY_REGEX.source)]),
                zip: new forms_1.FormControl("", [forms_1.Validators.pattern(narwhal_service_1.NarwhalService.ZIP_CODE_REGEX.source)]),
                phone_number: new forms_1.FormControl("", [forms_1.Validators.required, forms_1.Validators.pattern(narwhal_service_1.NarwhalService.PHONE_REGEX.source)]),
                shirt_size_id: new forms_1.FormControl("", [forms_1.Validators.required, forms_1.Validators.pattern(narwhal_service_1.NarwhalService.NONEMPTY_REGEX.source)])
            }, { validator: forms_1.Validators.compose([
                    this.getAllSetOrNotValidator(['street', 'city', 'state', 'zip']),
                    this.getRequiredEmptyIfNoneOthersSetValidator('suite', ['street', 'city', 'state', 'zip'])
                ]) }),
            user: this.fb.group({
                email: new forms_1.FormControl("", [forms_1.Validators.required, forms_1.Validators.pattern(narwhal_service_1.NarwhalService.EMAIL_REGEX.source)]),
                password: new forms_1.FormControl("", [forms_1.Validators.required, forms_1.Validators.pattern(narwhal_service_1.NarwhalService.PASSWORD_REGEX.source)]),
                password_confirmation: new forms_1.FormControl("", [forms_1.Validators.required])
            }, { validator: this.getEqualsValidator('password', 'password_confirmation') })
        });
        this.error = null;
    };
    RegisterController.prototype.getAllSetOrNotValidator = function (keys) {
        if (keys.length < 2) {
            throw new Error("Must be bound to at least two fields.");
        }
        return (function (group) {
            var anySet = false;
            for (var i = 0; i < keys.length; i++) {
                anySet = anySet || !!group.controls[keys[i]].value;
            }
            var result = {};
            if (anySet) {
                for (var i = 0; i < keys.length; i++) {
                    if (!group.controls[keys[i]].value) {
                        result[1] = group.controls[keys[i]].setErrors({ notSamePresence: true });
                    }
                }
            }
            return {};
        });
    };
    RegisterController.prototype.getRequiredEmptyIfNoneOthersSetValidator = function (key, others) {
        if (others.length < 2) {
            throw new Error("Must be bound to at least two fields.");
        }
        return function (group) {
            var anySet = false;
            for (var i = 0; i < others.length; i++) {
                anySet = anySet || !!group.controls[others[i]].value;
            }
            if (!anySet && !!group.controls[key].value) {
                group.controls[key].setErrors({ notSamePresence: true });
            }
            return {};
        };
    };
    RegisterController.prototype.getEqualsValidator = function (passwordKey, passwordConfirmationKey) {
        return function (group) {
            var passwordInput = group.controls[passwordKey];
            var passwordConfirmationInput = group.controls[passwordConfirmationKey];
            if (passwordInput.value !== passwordConfirmationInput.value) {
                passwordConfirmationInput.setErrors({ notEquivalent: true });
            }
        };
    };
    RegisterController.prototype.register = function (value, isValid) {
        var _this = this;
        this.submitted = true;
        if (!isValid) {
            this.error = "Please correct the issues above before submitting.";
        }
        else {
            this.error = null;
            console.log("Clicked");
            this.narwhalService.postRegistration(this.registrationFormGroup.value).catch(function (error) { _this.error = error; console.log(error); });
        }
    };
    RegisterController = __decorate([
        core_1.Component({
            selector: 'register-form',
            template: "\n    <div>\n        <form [formGroup]=\"registrationFormGroup\" (ngSubmit)=\"register(registrationFormGroup.value,registrationFormGroup.valid)\">\n            <div formGroupName=\"angler\">\n                <div class=\"form-group col-xs-8\">\n                    <label>First Name</label>\n                    <input type=\"text\" [formControl]=\"registrationFormGroup.controls.angler.controls.first_name\" placeholder=\"First Name\">\n                    <small *ngIf=\"(submitted || !registrationFormGroup.controls.angler.controls.first_name.pristine) && !registrationFormGroup.controls.angler.controls.first_name.valid\" class=\"text-danger\">Required.</small>\n                </div>\n                <div class=\"form-group col-xs-8\">\n                    <label>Last Name</label>\n                    <input type=\"text\" [formControl]=\"registrationFormGroup.controls.angler.controls.last_name\" placeholder=\"Last Name\">\n                    <small *ngIf=\"(submitted || !registrationFormGroup.controls.angler.controls.last_name.pristine) && !registrationFormGroup.controls.angler.controls.last_name.valid\" class=\"text-danger\">Required.</small>\n                </div>\n                <div class=\"form-group col-xs-8\">\n                    <label>Street Address</label>\n                    <input type=\"text\" [formControl]=\"registrationFormGroup.controls.angler.controls.street\" placeholder=\"Street Address\">\n                    <small *ngIf=\"(submitted || !registrationFormGroup.controls.angler.controls.street.pristine) && !registrationFormGroup.controls.angler.controls.street.valid\" class=\"text-danger\">Required if address given.</small>\n                </div>\n                <div class=\"form-group col-xs-8\">\n                    <label>Suite/Appt.</label>\n                    <input type=\"text\" [formControl]=\"registrationFormGroup.controls.angler.controls.suite\" placeholder=\"Suite/Appt.\">\n                    <small *ngIf=\"(submitted || !registrationFormGroup.controls.angler.controls.suite.pristine) && !registrationFormGroup.controls.angler.controls.suite.valid\" class=\"text-danger\">Omit if no address given.</small>\n                </div>\n                <div class=\"form-group col-xs-8\">\n                    <label>City</label>\n                    <input type=\"text\" [formControl]=\"registrationFormGroup.controls.angler.controls.city\" placeholder=\"City\">\n                    <small *ngIf=\"(submitted || !registrationFormGroup.controls.angler.controls.city.pristine) && !registrationFormGroup.controls.angler.controls.city.valid\" class=\"text-danger\">Required if address given.</small>\n                </div>\n                <div class=\"form-group col-xs-8\">\n                \t<label>State</label>\n\t\t\t\t\t<select name=\"state\" formControlName=\"state\">\n\t\t\t\t\t\t<option value=\"\" selected>--Select a State--</option>\n\t\t\t\t\t\t<option *ngFor=\"let state of states\" [ngValue]=\"state.abbreviation\">{{state.name}}</option>\n\t\t\t\t\t</select>\n\t\t\t\t\t<small *ngIf=\"(submitted || !registrationFormGroup.controls.angler.controls.state.pristine) && !registrationFormGroup.controls.angler.controls.state.valid\" class=\"text-danger\">Required if address given.</small>\n\t\t\t\t</div>\n                <div class=\"form-group col-xs-8\">\n                    <label>Zip Code</label>\n                    <input type=\"text\" [formControl]=\"registrationFormGroup.controls.angler.controls.zip\" placeholder=\"Zip Code\">\n                    <small *ngIf=\"(submitted || !registrationFormGroup.controls.angler.controls.zip.pristine) && !registrationFormGroup.controls.angler.controls.zip.valid\" class=\"text-danger\">5-Digit postal code. Required if address given.</small>\n                </div>\n                <div class=\"form-group col-xs-8\">\n                    <label>Phone Number</label>\n                    <input type=\"text\" [formControl]=\"registrationFormGroup.controls.angler.controls.phone_number\" placeholder=\"Phone Number\">\n                    <small *ngIf=\"(submitted || !registrationFormGroup.controls.angler.controls.phone_number.pristine) && !registrationFormGroup.controls.angler.controls.phone_number.valid\" class=\"text-danger\">Format: XXX-XXX-XXXX.</small>\n                </div>\n                <div class=\"form-group col-xs-8\">\n                    <label>Shirt Size</label>\n                    <select name=\"shirtSize\" formControlName=\"shirt_size_id\">\n                        <option value=\"\" selected>--Select a Shirt Size--</option>\n                        <option *ngFor=\"let size of shirtSizes\" [value]=\"size.id\">{{size.shirt_size_option}}</option>\n                    </select>\n                    <small *ngIf=\"(submitted || !registrationFormGroup.controls.angler.controls.shirt_size_id.pristine) && !registrationFormGroup.controls.angler.controls.shirt_size_id.valid\" class=\"text-danger\">Required.</small>\n                </div>\n            </div>\n            <div formGroupName=\"user\">\n                <div class=\"form-group col-xs-8\">\n                    <label>E-Mail Address</label>\n                    <input type=\"text\" formControlName=\"email\" placeholder=\"E-Mail Address\">\n                    <small *ngIf=\"(submitted || !registrationFormGroup.controls.user.controls.email.pristine) && !registrationFormGroup.controls.user.controls.email.valid\" class=\"text-danger\">Valid E-mail required.</small>\n                </div>\n                <div class=\"form-group col-xs-8\">\n                    <label>Password</label>\n                    <input type=\"password\" formControlName=\"password\" placeholder=\"Create Password\">\n                    <small *ngIf=\"(submitted || !registrationFormGroup.controls.user.controls.password.pristine) && !registrationFormGroup.controls.user.controls.password.valid\" class=\"text-danger\">Required. At least 8 characters.</small>\n                </div>\n                <div class=\"form-group col-xs-8\">\n                    <label>Re-Type Password</label>\n                    <input type=\"password\" formControlName=\"password_confirmation\" placeholder=\"Re-Type Password\">\n                    <small *ngIf=\"(submitted || !registrationFormGroup.controls.user.controls.password_confirmation.pristine) && !registrationFormGroup.controls.user.controls.password_confirmation.valid\" class=\"text-danger\">Required. Must match password.</small>\n                </div>\n            </div>\n            <div class=\"form-group col-xs-8\">\n\t\t\t\t<button type=\"submit\" value=\"Submit\">Submit</button>\n\t\t\t\t<small class=\"text-danger\">{{error}}</small>\n\t\t\t</div>\n        </form>\n    </div>\n  ",
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, narwhal_service_1.NarwhalService])
    ], RegisterController);
    return RegisterController;
}());
exports.RegisterController = RegisterController;
//# sourceMappingURL=register-form.component.js.map