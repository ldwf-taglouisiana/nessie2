import { Component, OnInit } from '@angular/core';
import { NarwhalService } from './../services/narwhal.service'
import {
	FormGroup, FormControl, Validators, Validator, FormBuilder, ReactiveFormsModule, FormsModule, ValidatorFn,
	AbstractControl
} from '@angular/forms';

@Component({
  selector: 'register-form',
  template: `
    <div>
        <form [formGroup]="registrationFormGroup" (ngSubmit)="register(registrationFormGroup.value,registrationFormGroup.valid)">
            <div formGroupName="angler">
                <div class="form-group col-xs-8">
                    <label>First Name</label>
                    <input type="text" [formControl]="registrationFormGroup.controls.angler.controls.first_name" placeholder="First Name">
                    <small *ngIf="(submitted || !registrationFormGroup.controls.angler.controls.first_name.pristine) && !registrationFormGroup.controls.angler.controls.first_name.valid" class="text-danger">Required.</small>
                </div>
                <div class="form-group col-xs-8">
                    <label>Last Name</label>
                    <input type="text" [formControl]="registrationFormGroup.controls.angler.controls.last_name" placeholder="Last Name">
                    <small *ngIf="(submitted || !registrationFormGroup.controls.angler.controls.last_name.pristine) && !registrationFormGroup.controls.angler.controls.last_name.valid" class="text-danger">Required.</small>
                </div>
                <div class="form-group col-xs-8">
                    <label>Street Address</label>
                    <input type="text" [formControl]="registrationFormGroup.controls.angler.controls.street" placeholder="Street Address">
                    <small *ngIf="(submitted || !registrationFormGroup.controls.angler.controls.street.pristine) && !registrationFormGroup.controls.angler.controls.street.valid" class="text-danger">Required if address given.</small>
                </div>
                <div class="form-group col-xs-8">
                    <label>Suite/Appt.</label>
                    <input type="text" [formControl]="registrationFormGroup.controls.angler.controls.suite" placeholder="Suite/Appt.">
                    <small *ngIf="(submitted || !registrationFormGroup.controls.angler.controls.suite.pristine) && !registrationFormGroup.controls.angler.controls.suite.valid" class="text-danger">Omit if no address given.</small>
                </div>
                <div class="form-group col-xs-8">
                    <label>City</label>
                    <input type="text" [formControl]="registrationFormGroup.controls.angler.controls.city" placeholder="City">
                    <small *ngIf="(submitted || !registrationFormGroup.controls.angler.controls.city.pristine) && !registrationFormGroup.controls.angler.controls.city.valid" class="text-danger">Required if address given.</small>
                </div>
                <div class="form-group col-xs-8">
                	<label>State</label>
					<select name="state" formControlName="state">
						<option value="" selected>--Select a State--</option>
						<option *ngFor="let state of states" [ngValue]="state.abbreviation">{{state.name}}</option>
					</select>
					<small *ngIf="(submitted || !registrationFormGroup.controls.angler.controls.state.pristine) && !registrationFormGroup.controls.angler.controls.state.valid" class="text-danger">Required if address given.</small>
				</div>
                <div class="form-group col-xs-8">
                    <label>Zip Code</label>
                    <input type="text" [formControl]="registrationFormGroup.controls.angler.controls.zip" placeholder="Zip Code">
                    <small *ngIf="(submitted || !registrationFormGroup.controls.angler.controls.zip.pristine) && !registrationFormGroup.controls.angler.controls.zip.valid" class="text-danger">5-Digit postal code. Required if address given.</small>
                </div>
                <div class="form-group col-xs-8">
                    <label>Phone Number</label>
                    <input type="text" [formControl]="registrationFormGroup.controls.angler.controls.phone_number" placeholder="Phone Number">
                    <small *ngIf="(submitted || !registrationFormGroup.controls.angler.controls.phone_number.pristine) && !registrationFormGroup.controls.angler.controls.phone_number.valid" class="text-danger">Format: XXX-XXX-XXXX.</small>
                </div>
                <div class="form-group col-xs-8">
                    <label>Shirt Size</label>
                    <select name="shirtSize" formControlName="shirt_size_id">
                        <option value="" selected>--Select a Shirt Size--</option>
                        <option *ngFor="let size of shirtSizes" [value]="size.id">{{size.shirt_size_option}}</option>
                    </select>
                    <small *ngIf="(submitted || !registrationFormGroup.controls.angler.controls.shirt_size_id.pristine) && !registrationFormGroup.controls.angler.controls.shirt_size_id.valid" class="text-danger">Required.</small>
                </div>
            </div>
            <div formGroupName="user">
                <div class="form-group col-xs-8">
                    <label>E-Mail Address</label>
                    <input type="text" formControlName="email" placeholder="E-Mail Address">
                    <small *ngIf="(submitted || !registrationFormGroup.controls.user.controls.email.pristine) && !registrationFormGroup.controls.user.controls.email.valid" class="text-danger">Valid E-mail required.</small>
                </div>
                <div class="form-group col-xs-8">
                    <label>Password</label>
                    <input type="password" formControlName="password" placeholder="Create Password">
                    <small *ngIf="(submitted || !registrationFormGroup.controls.user.controls.password.pristine) && !registrationFormGroup.controls.user.controls.password.valid" class="text-danger">Required. At least 8 characters.</small>
                </div>
                <div class="form-group col-xs-8">
                    <label>Re-Type Password</label>
                    <input type="password" formControlName="password_confirmation" placeholder="Re-Type Password">
                    <small *ngIf="(submitted || !registrationFormGroup.controls.user.controls.password_confirmation.pristine) && !registrationFormGroup.controls.user.controls.password_confirmation.valid" class="text-danger">Required. Must match password.</small>
                </div>
            </div>
            <div class="form-group col-xs-8">
				<button type="submit" value="Submit">Submit</button>
				<small class="text-danger">{{error}}</small>
			</div>
        </form>
    </div>
  `,
})
export class RegisterController implements OnInit {

    private registrationFormGroup : FormGroup;
    private error: string;
    private shirtSizes : Object[] = [];
    private states : Object[] = NarwhalService.STATES;
    private submitted : boolean = false;

    constructor(private fb: FormBuilder, private narwhalService : NarwhalService){ }

    ngOnInit() {
        this.narwhalService.getCombinedOptionsData('shirt_sizes').then((json) => this.shirtSizes = json);
        this.registrationFormGroup = this.fb.group({
            angler : this.fb.group({
                first_name: new FormControl("",[Validators.required,Validators.pattern(NarwhalService.NONEMPTY_REGEX.source)]),
                last_name: new FormControl("",[Validators.required,Validators.pattern(NarwhalService.NONEMPTY_REGEX.source)]),
                street: new FormControl("",[Validators.pattern(NarwhalService.NONEMPTY_REGEX.source)]),
                suite: new FormControl("",[Validators.pattern(NarwhalService.NONEMPTY_REGEX.source)]),
                city: new FormControl("",[Validators.pattern(NarwhalService.NONEMPTY_REGEX.source)]),
                state: new FormControl("",[Validators.pattern(NarwhalService.NONEMPTY_REGEX.source)]),
                zip: new FormControl("",[Validators.pattern(NarwhalService.ZIP_CODE_REGEX.source)]),
                phone_number: new FormControl("",[Validators.required,Validators.pattern(NarwhalService.PHONE_REGEX.source)]),
	       		shirt_size_id: new FormControl("",[Validators.required,Validators.pattern(NarwhalService.NONEMPTY_REGEX.source)])
            }, { validator: Validators.compose([
				this.getAllSetOrNotValidator(['street','city','state','zip']),
            	this.getRequiredEmptyIfNoneOthersSetValidator('suite',['street','city','state','zip'])
			])}),
            user : this.fb.group({
                email: new FormControl("",[Validators.required,Validators.pattern(NarwhalService.EMAIL_REGEX.source)]),
                password: new FormControl("",[Validators.required,Validators.pattern(NarwhalService.PASSWORD_REGEX.source)]),
                password_confirmation: new FormControl("",[Validators.required])
            }, { validator: this.getEqualsValidator('password','password_confirmation') }
            )
        });
        this.error = null;
    }

	private getAllSetOrNotValidator(keys : string[]) : ValidatorFn {
		if (keys.length < 2){
			throw new Error("Must be bound to at least two fields.");
		}
	  	return ((group: FormGroup) => {
			var anySet : boolean = false;
			for (var i : number = 0; i < keys.length; i++){
				anySet = anySet || !!group.controls[keys[i]].value;
			}
			var result : any = {};
			if (anySet){
				for (var i : number = 0; i < keys.length; i++){
					if (!group.controls[keys[i]].value){
						result[1] = group.controls[keys[i]].setErrors({notSamePresence: true});
					}
				}
			}
			return {};
  		});
	}

	private  getRequiredEmptyIfNoneOthersSetValidator(key : string, others : string[]) : ValidatorFn {
		if (others.length < 2){
			throw new Error("Must be bound to at least two fields.");
		}
	  	return (group: FormGroup) => {
			var anySet : boolean = false;
			for (var i : number = 0; i < others.length; i++){
				anySet = anySet || !!group.controls[others[i]].value;
			}
			if (!anySet && !!group.controls[key].value){
				group.controls[key].setErrors({notSamePresence: true});
			}
			return {};
  		};
	}

    private  getEqualsValidator(passwordKey: string, passwordConfirmationKey: string) {
	  return (group: FormGroup) => {
		let passwordInput = group.controls[passwordKey];
		let passwordConfirmationInput = group.controls[passwordConfirmationKey];
		if (passwordInput.value !== passwordConfirmationInput.value) {
		  passwordConfirmationInput.setErrors({notEquivalent: true})
		}
	  }
	}

    register(value : any, isValid : boolean){
    	this.submitted = true;
    	if (!isValid){
    		this.error = "Please correct the issues above before submitting.";
    	}
    	else {
			this.error = null;
			console.log("Clicked");
			this.narwhalService.postRegistration(this.registrationFormGroup.value).catch((error) => {this.error = error; console.log(error)});
    	}
    }

}
